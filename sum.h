/*********************************************************************
This file is produced as a part of an educational course.

This is the "header" file for the 'mysum' function: it only contains
its "declaration" (or name and arguments), not its definition (what it
does)! It is used by any '.c' file(s) that need to use this function.

Copyright (C) 2021, Mohammad Akhlaghi <mohammad@akhlaghi.org>

This file is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This file is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this file. If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

double
mysum(double a, double b);
