Dummy C program to help in reading the Make manual
--------------------------------------------------

The GNU Make manual is very good, but only uses C program compilation
as examples. This makes it hard for people who are not familiar with C
to read it. This small repository is setup to help in that process
with a very basic and easy to understand multi-file C program to help
in playing with the Make manual.

To compile the demo program, simply run `make`, and to execute it, run
`./dummy-prog`.
