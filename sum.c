/*********************************************************************
This file is produced as a part of an educational course.

These '.c' files contain the main "definition" of functions (showing
exactly what the functions do), and are compiled separately to create
'.o' files that will later be merged (technically; "linked") together
to create a program.

Copyright (C) 2021, Mohammad Akhlaghi <mohammad@akhlaghi.org>

This file is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This file is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this file. If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

double
mysum(double a, double b)
{
  return a+b;
}
