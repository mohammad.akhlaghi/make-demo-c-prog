/*********************************************************************
This file is produced as a part of an educational course.

This is a special '.c' file: it contains the special 'main'
function. The operating system starts the execution of a program by
calling its 'main' function. In this dummy example, it is this 'main'
function that calls the two 'mysum' and 'mymultip' functions and
prints their values.

Copyright (C) 2021, Mohammad Akhlaghi <mohammad@akhlaghi.org>

This file is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This file is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this file. If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "sum.h"
#include "multiply.h"

int
main(void)
{
  /* Definitions */
  double a=2, b=3;

  /* Let the user know we started. */
  printf("\nDummy program to show how Make works!\n");

  /* Sum the two numbers and let the user know. */
  printf("Sum: %g\n", mysum(a,b));

  /* Multiply the two numbers and let the user know. */
  printf("Multiplication: %g\n", mymultiply(a, b));

  /* Return with the code for "success"! */
  return EXIT_SUCCESS;
}
