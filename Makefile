# This file is produced as a part of an educational course.
#
# Top-level Makefile to create the 'myprog' executable. Each '.c'
# source file comes with a header file '.h'. Together they are used to
# construct the '.o' files, and all the '.o' files are then merged
# together to construct the final program.
#
# Copyright (C) 2021, Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Gnuastro. If not, see <http://www.gnu.org/licenses/>.

dummy-prog: main.o multiply.o sum.o
	cc -o dummy-prog main.o multiply.o sum.o

main.o: main.c multiply.h sum.h
	cc -c main.c

multiply.o: multiply.c multiply.h
	cc -c multiply.c

sum.o: sum.c sum.h
	cc -c sum.c

clean:; rm -f *.o myprog
